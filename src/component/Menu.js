import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";

export default function Menu({ navigation }) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("ContagemdeCliques")}
      >
        <Text>Contagem de Cliques</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("ContagemdeCaracteres")}
      >
        <Text>Contagem de Caracteres</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("ListadeTexto")}
      >
        <Text>Lista de Texto</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("Saudacoes")}
      >
        <Text>Saudações Diferentes Idiomas</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("Botao")}
      >
        <Text>Aqui não tem nada</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  menu: {
    padding: 10,
    margin: 5,
    backgroundColor: "lightblue",
    borderRadius: 5,
  },
});
